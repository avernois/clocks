#include <ZoneOffset.h>
#include <WifiLedClock.h>

#include <ledstrip/WS2812bDisplay.h>

#include "SolidSquareFace.h"
#include "DiceFace.h"
#include "RainbowSquareFace.h"

#define NUM_LEDS 36
#define DATA_PIN 2

HTTPClient httpClient;
String host = "zone.crafting-labs.fr";

Zone zone(httpClient, host);

ClockFace<LedStripFrame<NUM_LEDS>>* rainbow = new RainbowSquareFace();
ClockFace<LedStripFrame<NUM_LEDS>>* solid = new SolidSquareFace();
ClockFace<LedStripFrame<NUM_LEDS>>* dice = new DiceFace();

WS2812bDisplay<DATA_PIN, NUM_LEDS> *display = new WS2812bDisplay<DATA_PIN, NUM_LEDS>();

WifiLedClock<LedStripFrame<NUM_LEDS>>* ledClock = new WifiLedClock<LedStripFrame<NUM_LEDS>>("square", solid, display, &zone);

void setup() {
    Serial.begin(115200);
    ledClock->addFace(rainbow);
    ledClock->addFace(dice);
    ledClock->setup();
}

void loop() {
    ledClock->loop();
}
