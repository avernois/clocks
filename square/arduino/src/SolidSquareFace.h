#ifndef __SOLIDSQUAREDISPLAY_H__
#define __SOLIDSQUAREDISPLAY_H__

#include "SquareFace.h"
#include "FastLED.h"

class SolidSquareFace : public SquareFace {
    public:
        LedStripFrame<NUM_LEDS> frameFor(const int hour, const int minute, const int second);
        void serialPrint(int hour, int minute, int second);

    private:
        CRGB colors[4] = { CRGB::Yellow, CRGB::Blue, CRGB::Green, CRGB::Red};
        void display_cadre(LedStripFrame<NUM_LEDS>* frame, const int CADRE[9], int number, CRGB baseColor);
        void fill_cadre(LedStripFrame<NUM_LEDS>* frame, const int CADRE[9], CRGB color);
};

#endif