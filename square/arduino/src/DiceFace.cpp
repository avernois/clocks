#include "DiceFace.h"

#define ZERO { 0, 0, 0, 0, 0, 0, 0, 0, 0}
#define UN   { 0, 0, 0, 0, 1, 0, 0, 0, 0}
#define DEUX { 0, 0, 1, 0, 0, 0, 1, 0, 0}
#define TROIS { 1, 0, 0, 0, 1, 0, 0, 0, 1}
#define QUATRE { 1, 0, 1, 0, 0, 0, 1, 0, 1}
#define CINQ { 1, 0, 1, 0, 1, 0, 1, 0, 1}
#define SIX { 1, 0, 1,  1, 0, 1, 1, 0, 1}
#define SEPT { 1, 0, 1, 1, 1, 1, 1, 0, 1}
#define HUIT { 1, 1, 1, 1, 0, 1, 1, 1, 1}
#define NEUF { 1, 1, 1, 1, 1, 1, 1, 1, 1}

const int DICE[10][9] = {
     ZERO , UN, DEUX, TROIS, QUATRE, CINQ, SIX, SEPT, HUIT, NEUF 
};

void DiceFace::fill_cadre(LedStripFrame<NUM_LEDS>* frame, const int CADRE[9], CRGB color) {
    for(int i = 0; i < 9; i++) {
            frame->setPixelAt(CADRE[i], color);
        }
}

void DiceFace::display_cadre(LedStripFrame<NUM_LEDS>* frame, const int CADRE[9], int number, CRGB baseColor) {
    CHSV hsvColor = rgb2hsv_approximate(baseColor);

    for (int i=0; i < 9; i++) {
        if(DICE[number][i] == 1) {
            frame->setPixelAt(CADRE[i], CHSV((hsvColor.h + (i%3)*255/12)%255, 255 , 255));
        } else {
            frame->setPixelAt(CADRE[i], CRGB::Black);
        }
    }
}

unsigned long offsetMillis = 0;
int previousSecond = 0;

LedStripFrame<NUM_LEDS> DiceFace::frameFor(int hour, int minute, int second) {
    LedStripFrame<NUM_LEDS> frame;

    unsigned long mill = 0;
    if(previousSecond !=second) {
        previousSecond = second;
        offsetMillis = millis();
    } else {
        mill = millis() - offsetMillis;
    }
    int minute_unit = minute%10 ;

    int baseHue = 255*(second*1000 + mill)*10/60000;
    display_cadre(&frame, CADRE[0], minute_unit, CHSV((baseHue)%255, 255, 255));

    int minute_decade = minute/10 ;
    display_cadre(&frame, CADRE[1], minute_decade, CHSV((255/4 + baseHue)%255, 255, 255));
    
    int hour_unit = hour%10 ;
    display_cadre(&frame, CADRE[2], hour_unit, CHSV((255/4*2 + baseHue)%255, 255, 255));
    
    int hour_decade = hour/10 ;
    display_cadre(&frame, CADRE[3], hour_decade, CHSV((255/4*3 + baseHue)%255, 255, 255));

    frame.setDuration(5);

    return frame;
}   