#include "RainbowSquareFace.h"

void RainbowSquareFace::fill_cadre(LedStripFrame<NUM_LEDS>* frame, const int CADRE[9], CRGB color) {
    for(int i = 0; i < 9; i++) {
            frame->setPixelAt(CADRE[i], color);
        }
}

void RainbowSquareFace::fill_cadre_gradient(LedStripFrame<NUM_LEDS>* frame, const int CADRE[9], CRGB color) {
    CHSV hsvColor = rgb2hsv_approximate(color);
    for(int i = 0; i < 9; i++) {
            frame->setPixelAt(CADRE[i], CHSV(hsvColor.h + (i%3)*15, 255 , 255));
        }
}

void RainbowSquareFace::display_cadre(LedStripFrame<NUM_LEDS>* frame, const int CADRE[9], int number, CRGB baseColor) {
    if(number == 9) {
        fill_cadre_gradient(frame, CADRE, baseColor);
        return;
    }

    if(number == 0) {
        fill_cadre(frame, CADRE, CRGB::Black);
        return;
    }

    int previouslyLit[9];
    int nbLit = 0;
    for(int i=0; i <9; i++) {
        if(frame->getPixelAt(CADRE[i])) {
            previouslyLit[nbLit] = i;
            nbLit++;
        }
    }
    
    if(nbLit > 0) {
        int toLitOff = random(0, nbLit);
        frame->setPixelAt(CADRE[previouslyLit[toLitOff]], CRGB::Black);
        nbLit = nbLit - 1;
    }

    CHSV hsvColor = rgb2hsv_approximate(baseColor);
    
    while(nbLit < number){
        int rand = random(0, 9);
        if (!frame->getPixelAt(CADRE[rand])) {
            frame->setPixelAt(CADRE[rand], CHSV(hsvColor.h + (rand%3)*15, 255 , 255));
            nbLit++;
        }
    }
}

LedStripFrame<NUM_LEDS> RainbowSquareFace::frameFor(int hour, int minute, int second) {
    LedStripFrame<NUM_LEDS> frame;
    int minute_unit = minute%10 ;
    display_cadre(&frame, CADRE[0], minute_unit, CHSV(16, 255, 255));

    int minute_decade = minute/10 ;
    display_cadre(&frame, CADRE[1], minute_decade, CHSV(16 + 64, 255, 255));
    
    int hour_unit = hour%10 ;
    display_cadre(&frame, CADRE[2], hour_unit, CHSV(16 + 128, 255, 255));
    
    int hour_decade = hour/10 ;
    display_cadre(&frame, CADRE[3], hour_decade, CHSV(16 + 192, 255, 255));

    frame.setDuration(1000);
    return frame;
}   