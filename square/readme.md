# Square: a TIX inspired clock

![a square clock, horizontal](square/images/2020.08.03-square-hor.1024px.jpg)
![a square clock, vertical](square/images/2020.08.03-square-vert.1024px.jpg)

It uses 4*9 ws2812b leds (from a 60leds/m strip) and is strongly inspired by the [TIX clock](https://www.tixclock.shop/history/).