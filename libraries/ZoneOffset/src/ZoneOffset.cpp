#include "ZoneOffset.h"
#include <LittleFS.h>
#include <ArduinoJson.h>

Zone::Zone(HTTPClient &client, String host) {
    this->httpClient = &client;
    this->host = config.getZoneTransitionHostOr(host);
    this->wantedZone = config.getZoneOr(this->DEFAULT_ZONE);
}

void writeInFile(String filename, String payload) {
    Serial.print("Writing: ");
    Serial.println(payload);
    Serial.print("in file ");
    Serial.println(filename);

    LittleFS.begin();
    File f  = LittleFS.open(filename, "w");
    f.print(payload);
    f.close();
    LittleFS.end();
}

int Zone::loadTransition(String zone) {
    String filename = filenameOfZone(zone);

    Serial.print("Downloading transitions for zone: ");
    Serial.println(zone);
    String zoneEncoded = String(zone);
    zoneEncoded.replace("/", "%2F");
    
    this->httpClient->begin(this->wifiClient, this->host, this->port, "/" + zoneEncoded);
    int httpCode = this->httpClient->GET();

    if(httpCode != 200) {
        Serial.print("Something went wrong: server reply with error: ");
        Serial.println(httpCode);
        Serial.println("Will try again in 60s.");
        this->nextUpdateTime = millis() + 10*1000;
        this->httpClient->end();
        return -1;
    }
    
    writeInFile(filename, httpClient->getString());
    this->wantedZone = zone;
    this->nextUpdateTime = millis() + this->updateInterval;
    this->httpClient->end();

    return 0;
}

void Zone::readTransition(String zone) {
    const size_t capacity = JSON_ARRAY_SIZE(6) + 6*JSON_OBJECT_SIZE(2) + 90;
    DynamicJsonDocument doc(capacity);
    String filename = filenameOfZone(zone);
    LittleFS.begin();
    if(LittleFS.exists(filename)) {
        File f = LittleFS.open(filename, "r");
        Serial.println("Reading transition from file");   
        String payload = f.readString();
        
        deserializeJson(doc, payload);

        for(int i=0; i < 5; i++) {
            this->transitions[i] = {doc[i]["epoch"], doc[i]["offset"] };
        }
        this->loadedZone = zone;
        this->config.setZone(zone);
        this->currentTransitionIdx = 0;
    } else {
        Serial.println("Read Transitions: file does not exists");
    }
    LittleFS.end();
}

int Zone::getOffsetAt(unsigned long epoch) {
    unsigned long transitionEpoch = transitions[(currentTransitionIdx + 1)%5].epoch;
    if (transitionEpoch != 0 && epoch >= transitionEpoch) {
        currentTransitionIdx = (currentTransitionIdx + 1)%5;
        Serial.print("Changing transition: ");
        Serial.println(transitions[currentTransitionIdx].offset);
    }

    return transitions[currentTransitionIdx].offset;
}

void Zone::setZone(String zone) {
    this->wantedZone = zone;
    this->nextUpdateTime = millis();
}

String Zone::currentZone() {
    return this->loadedZone;
}

void Zone::updateTransition() {
    if(needToBeUpdated()) {
        Serial.println("Transition needs to be updated");
        int res = this->loadTransition(this->wantedZone);
        if (res == 0) {
            this->readTransition(this->wantedZone);
        }
    }
}

boolean Zone::needToBeUpdated() {
    return (millis() > this->nextUpdateTime);
}

String Zone::filenameOfZone(String zone) {
    return zone + ".json";
}

void Zone::changeZoneTransitionHost(String newHost){
    this->config.setZoneTransitionHost(host);
    this->host = newHost;
}

String Zone::getZoneTransitionHost() {
    return this->host;
}