# Libraries

This directory contains libraries develop to ease the creation of code to run each clock. The main goal is that each clock would only have to have code specific to it's way of displaying time.

Currently, there are two:
* ZoneOffset : it manages the timezone and is able to retrieve the rules for offset change (like daylight saving time) for a given zone. The clock will then be able to automatic change offsent when needed (with the help of [Zone Transition server](https://gitlab.com/avernois/zone-transition))).
* BaseClock : all the plumbing for my wifi connected clocks to work such as:
  * wifi connection: ask for wifi credential if it can't connect to a network
  * reset configuration by pushing "flash" button for more than 10 seconds
  * mDNS configuration
  * retrieve current time through sNTP
  * TimeZone configuration, offset for that zone will be manage automatically using ZoneOffset
  * basic display

I use (platform.io) to manage external dependencies and build. 

Note: ZoneOffset and BaseClock are not publish in the platform.io registry.


## API documentation

### POST /display

```
{
    "strip":
        [ {"color": "#FF0000", "led": 0 },
          {"color": "#00FF00", "led": 1 },
	        {"color": "#0000FF", "led": 3 }
        ],
    "duration": 2000
}
```
Where:
  * ``duration``: the frame will be displayed for that amount of time in milliseconds.
  * ``strip``: described the state of each led in the led strip.
    * ``color``: the color as RGB in hexa
    * ``led``: the number of the led

Note: if a led in the strip is not assigned to a color, it will be assigned to #OOOOOO.

This will put the given frame in a queue to be displayed on a first in first out base.
Frame will be displayed for the given `duration` time, expressed in milliseconds.

Note: the size of the queue ``MAX_FRAMES`` is set in ``BaseClock/src/DisplayQueue.h``.

Note: currently, the given frame will not be displayed as given. Each color will be added to the color that the display would have displayed otherwise.