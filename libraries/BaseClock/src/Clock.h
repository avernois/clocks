#ifndef __CLOCK_H__
#define __CLOCK_H__

#include "ConfigServer.h"

class Clock {
    public:
        virtual void begin();
        virtual void update();
        virtual int getHours();
        virtual int getMinutes();
        virtual int getSeconds();

        virtual void addConfigRoute(ConfigServer* server);
};

#endif
