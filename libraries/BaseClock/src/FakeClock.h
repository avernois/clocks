#ifndef __FAKECLOCK__
#define __FAKECLOCK__

#include "Clock.h"

class FakeClock : public Clock {
    public:
        void begin() {};
        void update() {
            second ++;
            if(second == 60) {
                second = 0;
                minute ++;
            }
            if(minute == 60) {
                minute = 0;
                hour = (hour + 1)%24;
            }
        };

        int getHours()   { return hour; };
        int getMinutes() { return minute; };
        int getSeconds() { return second; };

        void addConfigRoute(ConfigServer* server) {};

    private:
        int second = 0;
        int minute = 0;
        int hour = 0;

};
#endif