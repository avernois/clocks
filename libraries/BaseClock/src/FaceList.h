#ifndef __FACE_LIST_H__
#define __FACE_LIST_H__

#include "ClockFace.h"
#include <ConfigServer.h>

template <class FRAME>
class FaceList {
    public:
        void addFace(ClockFace<FRAME> *face) {
            faces[nbFaces] = face;
            nbFaces = (nbFaces + 1)%MAX_FACES;
        }

        void addFaceRoute(ConfigServer* server) {
            server->on("/face/next", HTTP_GET, nextFace());

            for(int faceId = 0; faceId < this->nbFaces; faceId ++) {
                String route = "/face/";
                route += faceId;
                server->on(route.c_str(), HTTP_GET, changeFace());
            }
        }

        ClockFace<FRAME>* current() {
            return faces[currentFace];
        }

    private:
        const int MAX_FACES = 24;
        ClockFace<FRAME>** faces = new ClockFace<FRAME>*[MAX_FACES];
        int currentFace = 0;
        int nbFaces = 0;

        ArRequestHandlerFunction nextFace() {
            return [&](AsyncWebServerRequest *request){
                this->currentFace = (this->currentFace + 1) % this->nbFaces;
                String msg = "change to face ";
                msg += this->currentFace;
                request->send(200, "text/plain", msg);
            };
        };

        ArRequestHandlerFunction changeFace() {  
            return [&](AsyncWebServerRequest *request){
                String url = request->url();
                int index = url.lastIndexOf("/");
                if(index > 0 && index < url.length()){
                    String s_faceNumber = url.substring(index+1);
                    this->currentFace = s_faceNumber.toInt();

                    String msg = "change to face ";
                    msg += this->currentFace;
                    request->send(200, "text/plain", msg);
                }
                
                request->send(404, "text/plain");
            };
        };
};

#endif
