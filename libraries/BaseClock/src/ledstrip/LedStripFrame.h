#ifndef __LED_STRIP_FRAME_H__
#define __LED_STRIP_FRAME_H__

#include <Frame.h>

#include <FastLED.h>
#include <AsyncJson.h>


template <int NB_LEDS>
class LedStripFrame : public Frame {
    

    public:
        const int nbLeds = NB_LEDS;
        
        LedStripFrame() : Frame() {
            strip = new CRGB[this->nbLeds];
            fill_solid( &(strip[0]), nbLeds, CRGB::Black);
        }

        LedStripFrame(JsonVariant json):LedStripFrame() {
            JsonObject object = json.as<JsonObject>();
            JsonArray array = object["strip"].as<JsonArray>();

            for(JsonVariant v : array) {
                String color = v["color"].as<String>();
                long number = (long) strtol( &color[1], NULL, 16 );
                this->setPixelAt(v["led"].as<int>(), CRGB(number));
            }
            this->setDuration(object["duration"].as<int>());
        }

        void setPixelAt(int position, CRGB color) {
            if(position < this->nbLeds)
                strip[position] = color;
        }

        CRGB getPixelAt(int position) {
            return strip[position];
        }

        void add(LedStripFrame *frame) {
            for(int i=0; i < this->nbLeds; i++) {
                strip[i] += frame->strip[i];
            }
        }

        void clear() {
            for(int i=0; i < this->nbLeds; i++) {
                this->strip[i] = CRGB::Black;
            }
            this->setDuration(0);
        }

        ~LedStripFrame() {
            delete strip;
        }



    private:
        CRGB* strip;

};

#endif
