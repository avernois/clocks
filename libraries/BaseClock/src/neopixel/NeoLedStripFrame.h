#ifndef __NEOLED_STRIP_FRAME_H__
#define __NEOLED_STRIP_FRAME_H__

#include <Frame.h>
#include <AsyncJson.h>
#include <Adafruit_NeoPixel.h>

template <int NB_LEDS>
class NeoLedStripFrame : public Frame {
    
    public:
        const int nbLeds = NB_LEDS;
        
        NeoLedStripFrame() : Frame() {
            for (int i = 0; i < NB_LEDS; i++)
            {
                strip[i] = 0;
            } 
        }

        NeoLedStripFrame(JsonVariant json):NeoLedStripFrame() {
            JsonObject object = json.as<JsonObject>();
            JsonArray array = object["strip"].as<JsonArray>();

            for(JsonVariant v : array) {
                String color = v["color"].as<String>();
                int hexValue = (int) strtol( &color[1], NULL, 16 );
                uint8_t r = ((hexValue >> 16) & 0xFF); 
                uint8_t g = ((hexValue >> 8) & 0xFF);
                uint8_t b = ((hexValue) & 0xFF) ;
                this->setPixelRGBAt(v["led"].as<int>(), r, g, b);
            }
            this->setDuration(object["duration"].as<int>());
        }

        void setPixelHSVAt(int position, uint16_t hue, uint8_t sat = 255, uint8_t val = 255) {
            if(position < this->nbLeds)
                strip[position] = Adafruit_NeoPixel::ColorHSV(hue, sat, val);
        }

        void setPixelRGBAt(int position, uint8_t red, uint8_t green = 255, uint8_t blue = 255) {
            if(position < this->nbLeds)
                strip[position] = Adafruit_NeoPixel::Color(red, green, blue);
        }

        uint32_t getPixelAt(int position) {
            return strip[position];
        }

        void add(NeoLedStripFrame *frame) {
            for(int i=0; i < this->nbLeds; i++) {
                strip[i] += frame->strip[i];
            }
        }

        void clear() {
            for(int i=0; i < this->nbLeds; i++) {
                this->strip[i] = 0;
            }
            this->setDuration(0);
        }

        ~NeoLedStripFrame() {
            delete strip;
        }

    private:
        uint32_t *strip = new uint32_t[NB_LEDS];

};

#endif
