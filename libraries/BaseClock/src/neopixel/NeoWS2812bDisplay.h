#ifndef __NEOWS2812b_DISPLAY_H__
#define __NEOWS2812b_DISPLAY_H__

#include <Adafruit_NeoPixel.h>
#include "NeoLedStripFrame.h"
#include "OTAFeedbackDisplay.h"


template<int PIN, int NB_LEDS>
class NeoWS2812bDisplay : public LedClockDisplay<NeoLedStripFrame<NB_LEDS>> {
    public:

        NeoWS2812bDisplay<PIN, NB_LEDS>(): LedClockDisplay<NeoLedStripFrame<NB_LEDS>>() {
            pixels = new Adafruit_NeoPixel(NB_LEDS, PIN, NEO_GRB);  
        };
        
        void begin() {
            pixels->begin();
        }

        void display(NeoLedStripFrame<NB_LEDS>* frame) {
            pixels->clear();
            for (int i=0; i < nbLeds; i++) {
                pixels->setPixelColor(i, frame->getPixelAt(i));
            }

            pixels->show();

            delay(frame->getDuration());
        };

        void reset() {
            fill(0);
            pixels->show();
        }

        void displayPowerOn() {
            fill(Adafruit_NeoPixel::ColorHSV(49151, 255, 47));
            pixels->show();
        }

        void displayConnectedToWifi() {
            fill(Adafruit_NeoPixel::ColorHSV(32767, 255, 47));
            pixels->show();
        }

        void displayNTPUpdated() {
            fill(Adafruit_NeoPixel::ColorHSV(16383, 255, 47));
            pixels->show();
        }

        void displayTransitionDownloaded() {
            fill(Adafruit_NeoPixel::ColorHSV(0, 255, 47));
            pixels->show();
        }

        void fill(uint32_t color) {
            for (int led =0; led < this->nbLeds; led++) {
                this->pixels->setPixelColor(led, color);
            }
            pixels->show();
        }

        void updateInProgress(int progress) {
            for(int led =0; led < nbLeds; led++) {
                if((led)*100/nbLeds <= progress)
                    this->pixels->setPixelColor(led, Adafruit_NeoPixel::Color(40, 40, 0));
                else
                    this->pixels->setPixelColor(led,  0);
            }
            pixels->show();
        };

        void updateEnded() {
            fill(Adafruit_NeoPixel::Color(0, 30, 0));
            pixels->show();
        };

        void updateFailed() {
            fill(Adafruit_NeoPixel::Color(30, 0, 0));
            pixels->show();
        };

    private:
        const int pin = PIN;
        const int nbLeds = NB_LEDS;
        Adafruit_NeoPixel *pixels;
};

#endif