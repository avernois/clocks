#ifndef __RESET_WIFI_CREDENTIAL_H__
#define __RESET_WIFI_CREDENTIAL_H__

#include <WiFiManager.h>

#define TEN_SECONDS 10 * 1000

class ResetWifiCredential {
    public:
        ResetWifiCredential(int buttonPin, WiFiManager *wifiManager) {
            this->resetWifiButtonPin = buttonPin;
            this->wifiManager = wifiManager;
        };

        void setup() {
            pinMode(this->resetWifiButtonPin, INPUT_PULLUP);;
        };
        void loop() {
            int buttonState = digitalRead(this->resetWifiButtonPin);
            if(buttonState == 0) {
                if(resetWifiButtonLastPressed == 0) {
                    resetWifiButtonLastPressed = millis();
                }    
                if (millis() - resetWifiButtonLastPressed > TEN_SECONDS) {
                    wifiManager->resetSettings();
                    ESP.restart();
                }
            } else {
                resetWifiButtonLastPressed = 0;
            }
        };

    private:
        WiFiManager *wifiManager;
        int resetWifiButtonPin = 0;
        int resetWifiButtonLastPressed = 0;
};

#endif