#include "Clock.h"

#include <WiFiUdp.h>
#include <NTPClient.h> 

#include "ZoneOffset.h"
#include <LittleFS.h>

class NTPZonedClock : public Clock {

    public:
    const unsigned long ntpUpdate = 3600 * 1000;

    NTPZonedClock(Zone* zone, String defaultNTPPool) {
        this->zone = zone;
        this->ntpPool = readNTPPoolOr(defaultNTPPool);
        this->ntpClient = new NTPClient(ntpUDP, this->ntpPool.c_str(), 0, ntpUpdate);
    }

    void begin() {
        ntpClient->begin();
        int retry = 0;
        while(ntpClient->update() == false && retry < 10) {
            retry++;
            delay(1000);
        }
    };

    void update() {
        if(WiFi.isConnected()) {
            ntpClient->update();
            zone->updateTransition();
        }

        ntpClient->setTimeOffset(0);
        int offset = zone->getOffsetAt(ntpClient->getEpochTime());
        
        ntpClient->setTimeOffset(offset);
    }

    int getHours() {
        return ntpClient->getHours();
    }

    int getMinutes() {
        return ntpClient->getMinutes();
    }

    int getSeconds() {
        return ntpClient->getSeconds();
    }

    void changeNTPPool(String newPool) {
        this->writeNTPPool(newPool);
        this->ntpPool = newPool;
        this->ntpClient->setPoolServerName(this->ntpPool.c_str());
    }


    String getNTPPool() {
        return this->ntpPool;
    }

    void addConfigRoute(ConfigServer* server) {
        server->on("/clock", HTTP_GET, [&] (AsyncWebServerRequest *request) {
            String message;
            if (request->hasParam("pool")) {
                message = request->getParam("pool")->value();
                this->changeNTPPool(request->getParam("pool")->value());
            } else {
                message = this->getNTPPool();
            }
            request->send(200, "text/plain", message);
        });

        server->on("/zone", HTTP_GET, [&] (AsyncWebServerRequest *request) {
            String message;
            if (request->params() == 0 ) {
                message = "Zone: " + this->zone->currentZone();
                message += "\n";
                message += "Host: " + this->zone->getZoneTransitionHost();
            }

            if (request->hasParam("name")) {
                message = request->getParam("name")->value();
                this->zone->setZone(request->getParam("name")->value());
            } 

            if (request->hasParam("host")) {
                message = request->getParam("host")->value();
                this->zone->changeZoneTransitionHost(request->getParam("host")->value());
            }


            request->send(200, "text/plain", message);
        });
}
    private:
    Zone* zone;
    String ntpPool;
    WiFiUDP ntpUDP;
    NTPClient* ntpClient;
    
    String readNTPPoolOr(String defaultNTPPool) {
        String ntpPool = defaultNTPPool;
        LittleFS.begin();
        if(LittleFS.exists("ntppool.txt")) {
            File f = LittleFS.open("ntppool.txt", "r");
            ntpPool = f.readString();
            f.close();
        }
        LittleFS.end();
        return ntpPool;
    };

    void writeNTPPool(String ntpPool) {
        LittleFS.begin();
        File f = LittleFS.open("ntppool.txt", "w");
        f.print(ntpPool);
        f.close();
        LittleFS.end();

        this->ntpPool = ntpPool;
        Serial.println("Clock: ntpPool is set to: " + this->ntpPool );
    }
};
