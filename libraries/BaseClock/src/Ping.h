#ifndef __PING__
#define __PING__

#include "ConfigServer.h"
#include <asyncHTTPrequest.h>
#include <LittleFS.h>
#include <ArduinoJson.h>

class PingConfig {
    public:
        String toJsonString() {
            String config = "{";
            config += "\"url\":\"";
            config += this->url;
            config += "\", \"interval\":";
            config += this->interval / 1000;
            config += ", \"enabled\":";
            config += (enabled?"true":"false");
            config += "}";

            return config;
        }

        void fromJsonString(String config) {
            const size_t capacity = 1024;
            DynamicJsonDocument doc(capacity);
            deserializeJson(doc, config);
            unsigned long i = doc["interval"];
            this->interval = i * 1000;
            const char* url = doc["url"];
            this->url = url;
            this->enabled = doc["enabled"];
        }


    private:
        unsigned long interval = 2 * 60 * 1000;
        String url = "http://zone.crafting-labs.fr/ping";
        bool enabled = false;

    friend class Ping;
};

class Ping {
    public:
        void setup() {
            Serial.println("Init Ping");
            this->loadConfig();
            request.onReadyStateChange([&](void* opt, asyncHTTPrequest* request, int readyState){this->requestCB(opt, request, readyState);});
            nextCall = millis() + this->config.interval;
        }

        void loop() {
            if(!this->config.enabled) {
                return;
            }

            unsigned long now = millis(); 
            if((!requestInProgress) && (now > nextCall)) {
                sendRequest();
                requestInProgress = true;
            }
        }

        void addConfigRoute(ConfigServer* server) {
            server->on("/ping", HTTP_GET, [&] (AsyncWebServerRequest *request) {
                String message = this->config.toJsonString();
                request->send(200, "text/plain", message);
            });

            server->on("/ping", HTTP_POST, [&] (AsyncWebServerRequest *request) {
                if (request->hasParam("url", true)) {
                    String newUrl = request->getParam("url", true)->value().c_str();
                    if(!newUrl.isEmpty()) {
                        this->config.url = request->getParam("url", true)->value().c_str();
                    }
                }

                if (request->hasParam("interval", true)) {
                    unsigned long newInterval = request->getParam("interval", true)->value().toInt();
                    if(newInterval >= 60) {
                        this->config.interval = newInterval * 1000;
                    }
                }

                if (request->hasParam("enabled", true)) {
                    String enabled = request->getParam("enabled", true)->value();
                    if(!enabled.isEmpty()) {
                        if(enabled == "true") {
                            this->config.enabled = true;
                        } else {
                            this->config.enabled = false;
                        }
                    }
                }

                this->writeConfig();
                String message = this->config.toJsonString();
                Serial.println(message);
                request->send(200, "text/plain", message);
            });
        }


    private:
        enum    readyStates {
                readyStateUnsent = 0,           // Client created, open not yet called
                readyStateOpened =  1,          // open() has been called, connected
                readyStateHdrsRecvd = 2,        // send() called, response headers available
                readyStateLoading = 3,          // receiving, partial data available
                readyStateDone = 4} _readyState;
        asyncHTTPrequest request;
        bool requestInProgress = false;
        unsigned long nextCall = 0;
        String url = "http://zone.crafting-labs.fr/ping";
        String configFilename = "ping_config.json";
        PingConfig config;

        void sendRequest() {
            static bool requestOpenResult;

            if (request.readyState() ==  readyStateUnsent || request.readyState() == readyStateDone) {
                requestOpenResult = request.open("GET", this->config.url.c_str());

                if (requestOpenResult) {
                    request.send();
                } else {
                    Serial.println(F("Can't send bad request"));
                }
            }
            else {
                Serial.println(F("Can't send request"));
            }
        }

        void requestCB(void *optParm, asyncHTTPrequest *request, int readyState) {
            (void) optParm;

            if (readyState == readyStateDone) {
                if (request->responseHTTPcode() == 200) {
                    Serial.print(millis());
                    Serial.print(" ");
                    Serial.println(request->responseText());
                }
                requestInProgress = false;
                this->nextCall = millis() + this->config.interval;
            }
        }

        void writeConfig() {
            String config = this->config.toJsonString();
            LittleFS.begin();
            File f  = LittleFS.open(configFilename, "w");
            f.print(config);
            f.close();
            LittleFS.end();

            Serial.print("Writing config: ");
            Serial.println(config);
        }

        void loadConfig() {
            const size_t capacity = JSON_OBJECT_SIZE(2);
            DynamicJsonDocument doc(capacity);
            LittleFS.begin();
            if(LittleFS.exists(configFilename)) {
                File f = LittleFS.open(configFilename, "r");
                Serial.println("Reading ping configuration from file");   
                String payload = f.readString();
                Serial.print("Reading config: ");
                Serial.println(payload);
                this->config.fromJsonString(payload);
            } else {
                Serial.println("Read Transitions: file does not exists");
            }
            LittleFS.end();
            Serial.println(this->config.toJsonString());
        }
};

#endif