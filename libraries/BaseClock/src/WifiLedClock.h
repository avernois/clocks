#ifndef __WIFI_LED_CLOCK_H__
#define __WIFI_LED_CLOCK_H__

#define WEBSERVER_H
#include <WiFiManager.h>

#include <LedClockDisplay.h>
#include <ZoneOffset.h>
#include <ResetWifiCredential.h>
#include "Clock.h"
#include "NTPZonedClock.h"
#include "NetworkClockId.h"
#include "ConfigServer.h"
#include "ClockFace.h"
#include "DisplayQueue.h"

#include "OTA.h"
#include "OTAFeedbackDisplay.h"

#include "Ping.h"

#include "FaceList.h"

template <class FRAME>
class WifiLedClock {
    public:
        WifiLedClock(String defaultName, ClockFace<FRAME>* face, LedClockDisplay<FRAME>* display, Zone* zone)
            : WifiLedClock(defaultName, face, display, new NTPZonedClock(zone, "europe.pool.ntp.org")) {}
            
        WifiLedClock(String defaultName, ClockFace<FRAME>* face, LedClockDisplay<FRAME>* display, Zone* zone, String defaultNTPPool)
            : WifiLedClock(defaultName, face, display, new NTPZonedClock(zone, defaultNTPPool)) {}

        WifiLedClock(String defaultName, ClockFace<FRAME>* face, LedClockDisplay<FRAME>* display, Clock* clock) {
            this->display = display;
            this->faces->addFace(face);
            this->wifiReset = new ResetWifiCredential(0, &wifiManager);
            this->clock = clock;
            this->networkClockId = new NetworkClockId(defaultName);
            this->configServer = new ConfigServer(this->networkClockId);
            this->ota = new OTA(display);
        };

        void setup() {
            display->begin();
            display->displayPowerOn();
            
            wifiManager.autoConnect(this->networkClockId->getClockName().c_str());
            display->displayConnectedToWifi();
            WiFi.setAutoReconnect(true);
            WiFi.persistent(true);
            ping.setup();
            clock->begin();
            display->displayNTPUpdated();

            clock->update();
            display->displayTransitionDownloaded(); 

            display->reset();

            this->networkClockId->begin();
            
            this->configServer->on("/firmware", HTTP_POST, ota->onRequest(), ota->onUpload());
            this->faces->addFaceRoute(this->configServer);
            this->clock->addConfigRoute(this->configServer);
            this->display->addConfigRoute(this->configServer);
            ping.addConfigRoute(this->configServer);
            this->configServer->begin();  

            wifiReset->setup();
        }

        void loop() {
            ota->loop();
            if(ota->isUpdateInProgress()) {
                delay(500);
                return;
            }

            wifiReset->loop();

            FRAME timeFrame = this->faces->current()->frameFor(this->clock->getHours(), this->clock->getMinutes(), this->clock->getSeconds());
            display->updateDisplay(&timeFrame);

            ping.loop();

            if(WiFi.isConnected()) {
                this->networkClockId->update();
                this->clock->update();
            }
            
        }

        void addFace(ClockFace<FRAME>* face) {
            this->faces->addFace(face);
        }

        void addRoute(const char* path, WebRequestMethodComposite method, ArRequestHandlerFunction onRequest) {
            this->configServer->on(path, method, onRequest);
        }

    private:
        LedClockDisplay<FRAME>* display;
        FaceList<FRAME>* faces = new FaceList<FRAME>();
    
        Zone* zone;
        ResetWifiCredential *wifiReset;
        WiFiManager wifiManager;

        Clock* clock; 
        NetworkClockId* networkClockId;
        ConfigServer* configServer;
        OTA *ota;
        bool needToReboot = false;
        Ping ping;
};

#endif