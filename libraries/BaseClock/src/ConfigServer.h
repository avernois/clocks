#ifndef __CONFIG_SERVER_H__
#define __CONFIG_SERVER_H__

#include <ESP8266WebServer.h>
#define WEBSERVER_H
#include <ESPAsyncWebServer.h>

#include "NetworkClockId.h"
#include <AsyncJson.h>


class ConfigServer {

    public:
        ConfigServer(NetworkClockId* networkClockId) {
            this->networkClockId = networkClockId;
        }

        void begin();
        
        void on(const char* uri, WebRequestMethodComposite method, ArRequestHandlerFunction onRequest, ArUploadHandlerFunction onUpload);
        void on(const char* uri, WebRequestMethodComposite method, ArRequestHandlerFunction onRequest);
        void addHandler(AsyncWebHandler* handler);

    private:
        NetworkClockId* networkClockId;

        AsyncWebServer server = AsyncWebServer(80);

        void configureCORS(String origin);
};

void ConfigServer::begin() {
    server.on("/", HTTP_GET, [](AsyncWebServerRequest *request){
        request->send(200, "text/plain", "Hello, world");
    });

    server.on("/network", HTTP_GET, [&] (AsyncWebServerRequest *request) {
        String message;
        if (request->hasParam("name")) {
            message = request->getParam("name")->value();
            this->networkClockId->changeName(request->getParam("name")->value());
        } else {
            message = this->networkClockId->getClockName();
        }
        request->send(200, "text/plain", message);
    });


    server.on("/version", HTTP_GET, [&] (AsyncWebServerRequest *request) {
        String message;
        message = "{ \"version\": \"";
        message += VERSION;
        message += "\", \"type\": \"";
        message += CLOCKTYPE;
        message += "\"}";
        request->send(200, "text/plain", message);
    });

    configureCORS("*");

    server.begin();
}

void ConfigServer::configureCORS(String origin) {
    DefaultHeaders::Instance().addHeader("Access-Control-Allow-Origin", origin);
    server.onNotFound([](AsyncWebServerRequest *request) {
        if (request->method() == HTTP_OPTIONS) {
            request->send(200);
        } else {
            request->send(404);
        }
    });
}

void ConfigServer::on(const char* uri, WebRequestMethodComposite method, ArRequestHandlerFunction onRequest, ArUploadHandlerFunction onUpload) {
    server.on(uri, method, onRequest, onUpload);
}

void ConfigServer::on(const char* uri, WebRequestMethodComposite method, ArRequestHandlerFunction onRequest) {
    server.on(uri, method, onRequest);
}


void ConfigServer::addHandler(AsyncWebHandler* handler) {
    server.addHandler(handler);
}
#endif