# Linear: a clock on a line

Linear is a clock where time can be read on a line.

It uses 73 ws2812b leds. The pcb for electronic can be found in the [`pcb/clockk_ws2812b` directory](https://gitlab.com/avernois/clocks/-/tree/main/pcb/clock_ws2812b)


Linear comes in different variation based on the density of led in the leds strip. All variation uses the same arduino code to run.

## cylindrical.30

The tall one, based on a 30leds/m strip.


## linear.144

![a large linear clock](images/2020.07.20-linear_144.1024px.jpg)

The small one, based on a 144leds/m strip.

## linear.60

![a large linear clock](images/2019.09.28-linear_clock.039.1024px.jpg)

The large one, based on a 60leds/m strip.