#include "CirclesFace.h"

class ShiftedStartCirclesFace : public CirclesFace {

public:
    NeoLedStripFrame<NUM_LEDS> frameFor(const int hour, const int minute, const int second) {
        NeoLedStripFrame<NUM_LEDS> frame;

        int intensity = 128;
    
        int hue = (128 + hour*MAX_HUE/12)%MAX_HUE;

        for(int i=0; i < frame.nbLeds; i++) {
            frame.setPixelRGBAt(i, 0, 0, 0);
        }
        
        for(int dot=1; dot <= minute; dot++) {
            frame.setPixelHSVAt((outerLedIndex(dot) + hour*6)%NUM_LEDS, hue + ((dot-1)/5)*MAX_HUE/12, 255, intensity);
        }

        if(second > 0) { 
           frame.setPixelHSVAt((outerLedIndex((minute + second)) + hour*6)%NUM_LEDS, hue, 0, intensity);
        }
        frame.setPixelHSVAt(innerLedIndex(hour % 12), (hue)%MAX_HUE, 255, intensity);
        
        frame.setDuration(5);
        return frame;
    }
};