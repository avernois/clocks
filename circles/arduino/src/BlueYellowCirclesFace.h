#include "CirclesFace.h"

class BlueYellowCirclesFace : public CirclesFace {

public:
    NeoLedStripFrame<NUM_LEDS> frameFor(const int hour, const int minute, const int second) {
        
        int lowIntensity = 96;
        int highIntensity = 192;
        NeoLedStripFrame<NUM_LEDS> frame;

        uint16_t hour_hue_low     = offsettedHue(212*MAX_HUE/255);
        uint16_t hour_highlight_hue_low = offsettedHue(127*MAX_HUE/255);
        uint16_t minute_hue       = offsettedHue(170*MAX_HUE/255);
        uint16_t second_hue       = 0;
        uint16_t hour_dot_hue     = offsettedHue(64*MAX_HUE/255);
   
        for(int i=0; i< frame.nbLeds; i++) {
            frame.setPixelRGBAt(i, 0, 0, 0);
        }

        for(int i=0; i <= hour%12; i++) {
            frame.setPixelHSVAt(innerLedIndex(i), hour_highlight_hue_low, 255, lowIntensity);
        }

        for(int i=hour%12; i < 12; i++) {       
            frame.setPixelHSVAt(innerLedIndex(i), hour_hue_low, 255, lowIntensity);
        }

        for(int dot = 1; dot <= minute; dot++) {
            frame.setPixelHSVAt(outerLedIndex(dot + 5), minute_hue, 255, lowIntensity);
        }

       frame.setPixelHSVAt(innerLedIndex(hour), hour_dot_hue, 255, highIntensity);

        if(second != 0) {
            frame.setPixelHSVAt(outerLedIndex(second + 5), second_hue, 0, highIntensity);
        }

        frame.setDuration(5);
        
        return frame;
    }

    private:
        int hueSteps = 0;
        const int hueStepsChange = 100;

        int offsettedHue(int hue) {
            int hueOffset = hueSteps/100;

            return (hue + hueOffset) % MAX_HUE;
        } 
  
};