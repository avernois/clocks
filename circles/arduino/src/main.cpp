#include <ZoneOffset.h>
#include <WifiLedClock.h>
#include <neopixel/NeoWS2812bDisplay.h>
#include "CirclesFace.h"
#include "BlueYellowCirclesFace.h"
#include "ShiftedStartCirclesFace.h"
#include <FakeClock.h>

HTTPClient httpClient;
String host = "zone.crafting-labs.fr";

Zone zone(httpClient, host);

#define NUM_LEDS 72
#define DATA_PIN 4

ClockFace<NeoLedStripFrame<NUM_LEDS>>* face = new CirclesFace();
NeoWS2812bDisplay<DATA_PIN, NUM_LEDS> *display = new NeoWS2812bDisplay<DATA_PIN, NUM_LEDS>();
WifiLedClock<NeoLedStripFrame<NUM_LEDS>>* ledClock = new WifiLedClock<NeoLedStripFrame<NUM_LEDS>>(CLOCKTYPE, face, display, &zone);
                                                                                                 

void setup() {
    Serial.begin(115200);
    ledClock->addFace(new BlueYellowCirclesFace());
    ledClock->addFace(new ShiftedStartCirclesFace());
    ledClock->setup();
}

void loop() {
    ledClock->loop();
}