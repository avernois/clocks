#ifndef __have__CirclesClockDisplay_h__
#define __have__CirclesClockDisplay_h__

#include <ClockFace.h>
#include <neopixel/NeoLedStripFrame.h>

#define NUM_LEDS 72

class CirclesFace: public ClockFace<NeoLedStripFrame<NUM_LEDS>> {
    public:
        const uint16_t MAX_HUE = 65535;
        NeoLedStripFrame<NUM_LEDS> frameFor(const int hour, const int minute, const int second);

        int outerLedIndex(int dot) {
            return offsetLedIndex(outerIndex[(dot - 1)%5] + ((dot-1)/5)*6);
        }

        int innerLedIndex(int dot) {
            if(dot < 0)
                return offsetLedIndex(innerIndex + (12 + dot)*6);

            return offsetLedIndex(innerIndex + dot*6);
        }

    
    private:
        int hueOffset = 0;
        int previousSecond = 0;
        int millisOffset = 0;
        int outerIndex[5] = { 3, 4, 5, 1, 2 };
        int innerIndex = 0;

        int offsetLedIndex(int ledIndex) {
            int offset = 36;
            return (ledIndex + offset) % NUM_LEDS;
        }        
};

#endif