#include "CirclesFace.h"

NeoLedStripFrame<NUM_LEDS> CirclesFace::frameFor(const int hour, const int minute, const int second) {
    NeoLedStripFrame<NUM_LEDS> frame;

    int intensity = 128;

    int minuteHueOffset = hour*MAX_HUE/24;
    int hourHueOffset   = hour*MAX_HUE/24 + hour*MAX_HUE/12;
    
    frame.clear();
    frame.setPixelHSVAt(innerLedIndex(hour % 12), hourHueOffset, 255, intensity);

    for(int dot = 6; dot <= minute + 5; dot++) {
        frame.setPixelHSVAt(outerLedIndex(dot) %NUM_LEDS, minuteHueOffset + ((dot-1)/5)*MAX_HUE/12, 255, intensity);
    }

    if(second > 0) {
        frame.setPixelHSVAt(outerLedIndex(second + 5), 0, 0, intensity);
    }

    frame.setDuration(100);
    return frame;
}
