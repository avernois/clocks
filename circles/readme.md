# circles : kind of circular clock

*Circles* is an attempt a to recreate the feel of a desk clock with leds and wood. 
![circles](images/2024.08.28-circles.1.1024px.jpg)


Contrary to most other clocks in that repository, this one has its own pcb for controler and leds. They can be found in [pcb](pcb/).

## Licence
All code and design on this repository is placed under the Hippocratic 2.1 licence. See details in [LICENCE.md](../LICENCE.md).

You can read more about that licence on [the website firstdonoharm.dev](https://firstdonoharm.dev/).
