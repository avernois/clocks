# PCBs for *Circles* clock

*Circles* require 2 different pcbs.
This directory contains kicad 6 files.


They are made using kicad 6.

If you want to see/edit the schematics, you might need to get my [custom kicad components/footprint library](https://gitlab.com/avernois/kicad-lib) (and probably fix some directory links, as I did not manage to make them properly relative/independent).


## clock_ws2812b

This is for the controller. Only 1 is required

### Bill of material

* SW1: SMD tactile button 4.5x4.5x3mm
* SW2: SMD tactile button 4.5x4.5x3mm
* U1 : HT7333 (3,3V linear voltage regulator)
* U2 : esp-12E (or esp-12F)
* J1 : 5 pins header (to connect a micro USB breakout)
* J2 : 3 pins connector (to leds strip)
* J3 : 5 pins header (to rx/tx/gnd + optional external flash/reset button)
* C1 : 470uF tantalum capacitor type D
* C2 : 100nF smd 0805 (metric 2012)
* C3 : 100nF smd 0805 (metric 2012)
* C4 : 100uF smd 1206 (metric 3216)
* R1 : 10kO smd 0805 (metric 2012)

## circles

This is for the leds. 12 are needed.

### Bill of material

* D1 -> D6 : 5050 ws2812b leds 
* C1 -> C6 : 10nF smd 0805 (metric 2012)

