EESchema Schematic File Version 4
EELAYER 26 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 1 1
Title "Led strip connector"
Date "2017-12-05"
Rev ""
Comp "Crafting Labs"
Comment1 "This is made for Gouge"
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
$Comp
L crafting_labs:ws2812_strip L1
U 1 1 5A270EAB
P 4350 2550
F 0 "L1" H 4250 2900 60  0000 C CNN
F 1 "ws2812_strip" V 4150 2550 60  0000 C CNN
F 2 "crafting-labs:ws2812_strip" H 4550 2150 60  0001 C CNN
F 3 "" H 4550 2150 60  0000 C CNN
	1    4350 2550
	1    0    0    -1  
$EndComp
Wire Wire Line
	4650 2350 5250 2350
Wire Wire Line
	5000 2500 4650 2500
Wire Wire Line
	5000 2500 5000 2450
Wire Wire Line
	5000 2450 5250 2450
$Comp
L Connector:Conn_01x03_Female J1
U 1 1 5C2CD28F
P 5450 2450
F 0 "J1" H 5477 2426 50  0000 L CNN
F 1 "Conn_01x03" H 5477 2335 50  0000 L CNN
F 2 "crafting-labs:cable_conn_3x_smd" H 5450 2450 50  0001 C CNN
F 3 "~" H 5450 2450 50  0001 C CNN
	1    5450 2450
	1    0    0    -1  
$EndComp
$Comp
L power:PWR_FLAG #FLG0101
U 1 1 5C2CD970
P 4900 2800
F 0 "#FLG0101" H 4900 2875 50  0001 C CNN
F 1 "PWR_FLAG" H 4900 2973 50  0000 C CNN
F 2 "" H 4900 2800 50  0001 C CNN
F 3 "~" H 4900 2800 50  0001 C CNN
	1    4900 2800
	-1   0    0    1   
$EndComp
Wire Wire Line
	5250 2550 5250 2800
Wire Wire Line
	5250 2800 4900 2800
Connection ~ 4900 2800
Wire Wire Line
	4900 2800 4650 2800
$EndSCHEMATC
