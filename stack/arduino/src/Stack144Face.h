#ifndef __have__LinearClockDisplay73_h__
#define __have__LinearClockDisplay73_h__

#include <ClockFace.h>
#include <neopixel/NeoLedStripFrame.h>

#define NUM_LEDS 144

class Stack144Face: public ClockFace<NeoLedStripFrame<NUM_LEDS>> {
    public:
        Stack144Face();
        NeoLedStripFrame<NUM_LEDS> frameFor(const int hour, const int minute, const int second);

    private:
        const int MAX_HUE = 65535;
        typedef struct {
            int nbLeds;
            int *leds;
            int nbSteps;
            int *steps;
            int hueBase;
            int hueStep;
        } LedsStrip;

        LedsStrip secondStrip = 
            { .nbLeds = 60, .leds = new int[60], 
              .nbSteps = 4, .steps = new int[4] { 30, 15, 5, 1},
              .hueBase = 0, .hueStep = MAX_HUE/3/60
            };
        LedsStrip minuteStrip = 
            { .nbLeds = 60, .leds = new int[60], 
              .nbSteps = 4, .steps = new int[4] { 30, 15, 5, 1},
              .hueBase = MAX_HUE/3, .hueStep = MAX_HUE/3/60
            };
        LedsStrip hourStrip =
            { .nbLeds = 24, .leds = new int[24], 
              .nbSteps = 4, .steps = new int[4] { 12, 6, 3, 1},
              .hueBase = MAX_HUE/3*2, .hueStep = MAX_HUE/3/24
            };
        
        typedef enum { NONE, IN_PROGRESS, FINISHED, WAITING } TransitionStatus;

        typedef struct {
            int currentStep;
            TransitionStatus status;
        } TransitionState;

        typedef struct {
            int moveLength;
            int nbLeds;
            int speed;
        } TransitionInfo;

        const TransitionInfo transition5_60 = { 10, 5, 5};
        const TransitionInfo transition10_60 = {.moveLength = 5, .nbLeds = 5, .speed = 5};
        const TransitionInfo transition15_60 = {.moveLength = 15, .nbLeds = 15, .speed = 4};
        const TransitionInfo transition30_60 = {.moveLength = 30, .nbLeds = 30, .speed = 2};
        TransitionInfo transition0_60 = {.moveLength = 60, .nbLeds = 60, .speed = 1};
        TransitionInfo transition1_60 = {.moveLength = 1, .nbLeds = 1, .speed = 10};


        TransitionInfo transition0_24 = {.moveLength = 24, .nbLeds = 24, .speed = 2};
        TransitionInfo transition1_24 = {.moveLength = 1, .nbLeds = 1, .speed = 10};
        const TransitionInfo transition3_24 = {.moveLength = 3, .nbLeds = 3, .speed = 8};
        const TransitionInfo transition6_24 = {.moveLength = 6, .nbLeds = 6, .speed = 8};
        const TransitionInfo transition12_24 = {.moveLength = 12, .nbLeds = 12, .speed = 4};

        TransitionState secondStatus = {0, NONE};
        TransitionState minuteStatus = {0, NONE};
        TransitionState hourStatus = {0, NONE};

        int highIntensity = 63;
        int lowIntensity = 1;

        int hueOffset = 0;
        int hueOffsetSpeed = 15;

        void addLeds(NeoLedStripFrame<NUM_LEDS> *frame, int value, LedsStrip strip);
        void displaySecond(NeoLedStripFrame<NUM_LEDS> *frame, int value);
        void displayMinute(NeoLedStripFrame<NUM_LEDS> *frame, int value);
        void displayHour(NeoLedStripFrame<NUM_LEDS> *frame, int value);

        void addSegment(NeoLedStripFrame<NUM_LEDS> *frame, LedsStrip strip, int startPosition, int length);
        bool addTransitionSegment(NeoLedStripFrame<NUM_LEDS> *frame, LedsStrip strip, int toDisplay, int transitionStep, int nbLeds, int speed, int moveLength);
        bool addTransitionSegment(NeoLedStripFrame<NUM_LEDS> *frame, LedsStrip strip, int toDisplay, int transitionStep, TransitionInfo transition);
        void transition60(NeoLedStripFrame<NUM_LEDS> *frame, LedsStrip strip, TransitionState *transitionStatus, int toDisplay);
        void transition24(NeoLedStripFrame<NUM_LEDS> *frame, LedsStrip strip, TransitionState *transitionStatus, int toDisplay);

        void updateHueOffset();

        void initFrame(NeoLedStripFrame<NUM_LEDS> *frame);
};

#endif