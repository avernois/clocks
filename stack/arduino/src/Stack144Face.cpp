#include "Stack144Face.h"

int lastSecond = 0;
int lastMinute = 0;
int lastHour = 0;

Stack144Face::Stack144Face() {
    for(int led=0; led < 60; led++) {
        secondStrip.leds[led] = 84 + led;
        minuteStrip.leds[led] = 83 - led;
    }

    for(int led=0; led < 24; led++) {
        hourStrip.leds[led] = led;
    }
}

NeoLedStripFrame<NUM_LEDS> Stack144Face::frameFor(const int hour, const int minute, const int second) {
    NeoLedStripFrame<NUM_LEDS> frame;

    int secondToDisplay = second;
    int minuteToDisplay = minute;
    int hourToDisplay = hour;

    initFrame(&frame);

    switch(secondStatus.status) {
    case IN_PROGRESS:
        transition60(&frame, secondStrip, &secondStatus, secondToDisplay);
        break;
    case NONE:
        if(secondToDisplay != lastSecond) {
            secondStatus.status = IN_PROGRESS;
            secondStatus.currentStep = 0;    
            transition60(&frame, secondStrip, &secondStatus, secondToDisplay);
        } else {
            displaySecond(&frame, secondToDisplay);
            lastSecond = secondToDisplay;    
        }       
        break;
    case WAITING:
        displaySecond(&frame, lastSecond);
        break;
    case FINISHED:
        secondStatus.status = NONE;
        displaySecond(&frame, secondToDisplay);
        lastSecond = secondToDisplay;

        if(minuteStatus.status == WAITING) {
            minuteStatus.status = IN_PROGRESS;
            minuteStatus.currentStep = 0;
        }        
        break;
    default:
        break;
    }

    switch(minuteStatus.status) {
    case IN_PROGRESS:
        transition60(&frame, minuteStrip, &minuteStatus, minuteToDisplay);
        break;
    case NONE:
        if(minuteToDisplay != lastMinute) {
            minuteStatus.status = WAITING;
            minuteStatus.currentStep = 0;    
            displayMinute(&frame, lastMinute);
        } else {
            displayMinute(&frame, minuteToDisplay);
            lastMinute = minuteToDisplay;    
        }       
        break;
    case WAITING:
        displayMinute(&frame, lastMinute);
        break;
    case FINISHED:
        minuteStatus.status = NONE;
        displayMinute(&frame, minuteToDisplay);
        lastMinute = minuteToDisplay;

        if(hourStatus.status == WAITING) {
            hourStatus.status = IN_PROGRESS;
            hourStatus.currentStep = 0;
        }
        break;
    default:
        break;
    }

    switch(hourStatus.status) {
    case IN_PROGRESS:
        transition24(&frame, hourStrip, &hourStatus, hourToDisplay);
        break;
    case NONE:
        if(hourToDisplay != lastHour) {
            hourStatus.status = WAITING;
            hourStatus.currentStep = 0;    
            displayHour(&frame, lastHour);
        } else {
            displayHour(&frame, hourToDisplay);
            lastHour = hourToDisplay;    
        }       
        break;
    case WAITING:
        displayHour(&frame, lastHour);
        break;
    case FINISHED:
        hourStatus.status = NONE;
        displayHour(&frame, hourToDisplay);
        lastHour = hourToDisplay;
        break;
    default:
        break;
    }

    updateHueOffset();

    frame.setDuration(5);
    return frame;
}

bool Stack144Face::addTransitionSegment(NeoLedStripFrame<NUM_LEDS> *frame, LedsStrip strip, int toDisplay, int transitionStep, int nbLeds, int speed, int moveLength) {
    if(transitionStep < moveLength * speed) {
        addSegment(frame, strip, transitionStep/speed, nbLeds);
        addLeds(frame, toDisplay - nbLeds, strip);
        return true;
    } else {
        addLeds(frame, toDisplay, strip);
        return false;
    }
}

void Stack144Face::displaySecond(NeoLedStripFrame<NUM_LEDS> *frame, int value) {
    addLeds(frame, value, secondStrip);
}

void Stack144Face::displayMinute(NeoLedStripFrame<NUM_LEDS> *frame, int value) {
    addLeds(frame, value, minuteStrip);
}

void Stack144Face::displayHour(NeoLedStripFrame<NUM_LEDS> *frame, int value) {
    addLeds(frame, value, hourStrip);
}

void Stack144Face::addLeds(NeoLedStripFrame<NUM_LEDS> *frame, int value, LedsStrip strip) {
    int remaining = value;

    int lastLeds = strip.nbLeds;
    for(int i = 0; i < strip.nbSteps; i++) {
        int step = strip.steps[i];

        if(remaining / step >= 1) {
            for(int led = lastLeds - 1; led > lastLeds - 1 - step * (remaining/step); led--) {
                frame->setPixelHSVAt(strip.leds[led], hueOffset + strip.hueBase + led * strip.hueStep, 255, highIntensity);
            }
        }
        remaining -= step * (remaining/step);
        lastLeds -= step * (lastLeds/step - 1);
    }    
};

void Stack144Face::addSegment(NeoLedStripFrame<NUM_LEDS> *frame, LedsStrip strip, int startPosition, int length) {
    for(int i=startPosition; i < startPosition + length; i++) {
        frame->setPixelHSVAt(strip.leds[i], hueOffset + strip.hueBase + i * strip.hueStep, 255, highIntensity);
    }
}

bool Stack144Face::addTransitionSegment(NeoLedStripFrame<NUM_LEDS> *frame, LedsStrip strip, int toDisplay, int transitionStep, TransitionInfo transition) {
    return addTransitionSegment(frame, strip, toDisplay, transitionStep, transition.nbLeds, transition.speed, transition.moveLength);
}

void Stack144Face::transition60(NeoLedStripFrame<NUM_LEDS> *frame, LedsStrip strip, TransitionState *transitionStatus, int toDisplay) {

    switch (toDisplay)
    {
    case 0:
        transition0_60.nbLeds = 60 - transitionStatus->currentStep;
        if(!addTransitionSegment(frame, strip, toDisplay, transitionStatus->currentStep, transition0_60)) {
            transitionStatus->status = FINISHED;
        }
        break;
    case 5:
    case 20:
    case 35:
    case 50:
        if(!addTransitionSegment(frame, strip, toDisplay, transitionStatus->currentStep, transition5_60)) {
            transitionStatus->status = FINISHED;
        }
        break;
    case 10:
    case 25:
    case 40:
    case 55:
        if(!addTransitionSegment(frame, strip, toDisplay, transitionStatus->currentStep, transition10_60)) {
            transitionStatus->status = FINISHED;
        }
        break;
    case 15:
    case 45:
        if(!addTransitionSegment(frame, strip, toDisplay, transitionStatus->currentStep, transition15_60)) {
            transitionStatus->status = FINISHED;
        }

        break;
    case 30:
        if(!addTransitionSegment(frame, strip, toDisplay, transitionStatus->currentStep, transition30_60)) {
            transitionStatus->status = FINISHED;
        }
        break;
    default:
        transition1_60.moveLength = 5 - ((toDisplay - 1) % 5);
        if(!addTransitionSegment(frame, strip, toDisplay, transitionStatus->currentStep, transition1_60)) {
            transitionStatus->status = FINISHED;
        }
        break;
    }

    transitionStatus->currentStep++;
}

void Stack144Face::transition24(NeoLedStripFrame<NUM_LEDS> *frame, LedsStrip strip, TransitionState *transitionStatus, int toDisplay) {
    if(transitionStatus->status != IN_PROGRESS) {
            transitionStatus->currentStep = 0;
            transitionStatus->status = IN_PROGRESS;
    } else {
        transitionStatus->currentStep++;
    }

    switch (toDisplay)
    {
    case 0:
        transition0_24.nbLeds = 24 - transitionStatus->currentStep/transition0_24.speed;
        if(!addTransitionSegment(frame, strip, toDisplay, transitionStatus->currentStep, transition0_24)) {
            transitionStatus->status = FINISHED;
        }
        break;
    case 3:
    case 9:
    case 15:
        if(!addTransitionSegment(frame, strip, toDisplay, transitionStatus->currentStep, transition3_24)) {
            transitionStatus->status = FINISHED;
        }
        break;
    case 6:
    case 18:
        if(!addTransitionSegment(frame, strip, toDisplay, transitionStatus->currentStep, transition6_24)) {
            transitionStatus->status = FINISHED;
        }
        break;
    case 12:
        if(!addTransitionSegment(frame, strip, toDisplay, transitionStatus->currentStep, transition12_24)) {
            transitionStatus->status = FINISHED;
        }
        break;
    case 30:
        if(!addTransitionSegment(frame, strip, toDisplay, transitionStatus->currentStep, transition30_60)) {
            transitionStatus->status = FINISHED;
        }
        break;
    default:
        transition1_24.moveLength = 3 - ((toDisplay - 1) % 3);
        if(!addTransitionSegment(frame, strip, toDisplay, transitionStatus->currentStep, transition1_24)) {
            transitionStatus->status = FINISHED;
        }
        break;
    }
}

void Stack144Face::updateHueOffset() {
    hueOffset = (hueOffset + hueOffsetSpeed)%MAX_HUE;
}

void Stack144Face::initFrame(NeoLedStripFrame<NUM_LEDS> *frame) {
    for(int i=1; i < secondStrip.nbLeds; i++) {
        frame->setPixelHSVAt(secondStrip.leds[i], hueOffset + secondStrip.hueBase + i * secondStrip.hueStep, 255, lowIntensity);
        frame->setPixelHSVAt(minuteStrip.leds[i], hueOffset + minuteStrip.hueBase + i * minuteStrip.hueStep, 255, lowIntensity);
    }
    for(int i=1; i < hourStrip.nbLeds; i++) {
        frame->setPixelHSVAt(hourStrip.leds[i], hueOffset + hourStrip.hueBase + i * hourStrip.hueStep, 255, lowIntensity);
    }
}