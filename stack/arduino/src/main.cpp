#include <ZoneOffset.h>
#include <WifiLedClock.h>

#include <neopixel/NeoWS2812bDisplay.h>

#include "Stack144Face.h"

HTTPClient httpClient;
String host = "zone.crafting-labs.fr";

Zone zone(httpClient, host);

#define NUM_LEDS 144
#define DATA_PIN 4

ClockFace<NeoLedStripFrame<NUM_LEDS>>* face = new Stack144Face();
NeoWS2812bDisplay<DATA_PIN, NUM_LEDS> *display = new NeoWS2812bDisplay<DATA_PIN, NUM_LEDS>();
WifiLedClock<NeoLedStripFrame<NUM_LEDS>>* ledClock = new WifiLedClock<NeoLedStripFrame<NUM_LEDS>>("stack144", face, display, &zone);

void setup() {
    Serial.begin(230400);
    ledClock->setup();
}

void loop() {
    ledClock->loop();
}
