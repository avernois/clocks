# stack : another clock

![stack](images/2024.10.29-stack.1.1024px.jpeg)


# pcb
*Stack* is using *Circles* pcb, that can be found in [../circles/pcb/clock_ws2812b/]

# Leds

*Stack* is built around two different led strip connected together:
* 120 leds of a 144leds/m strip
* 24 leds of a 74leds/m strip

## Licence
All code and design on this repository is placed under the Hippocratic 2.1 licence. See details in [LICENCE.md](../LICENCE.md).

You can read more about that licence on [the website firstdonoharm.dev](https://firstdonoharm.dev/).
