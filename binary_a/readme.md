# binary a: a binary clock

*Binary a* displays time (hour, minute and second) in binary.

![binary a](images/2022.01.13-binary_a.1.1024px.jpg)

## Licence
All code and design on this repository is placed under the Hippocratic 2.1 licence. See details in [LICENCE.md](../LICENCE.md).

You can read more about that licence on [the website firstdonoharm.dev](https://firstdonoharm.dev/).
