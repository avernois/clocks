#ifndef __have__BinaryAFace_h__
#define __have__BinaryAFace_h__

#include <ledstrip/LedStripFrame.h>
#include <ClockFace.h>

#define NUM_LEDS 24

class BinaryAFace: public ClockFace<LedStripFrame<NUM_LEDS>> {
    public:
        LedStripFrame<NUM_LEDS> frameFor(const int hour, const int minute, const int second);

    private:
        const int max_intensity = 255;
        const int min_intensity = 40;
        const int fading_step = 8;
        int hue_offset = 0;
        int previous_second = 0;
        int previous_minute = 0;
        int previous_hour = 0;
        int fade = 0;
        int DIGIT[6][4] =  {
            { 19, 18, 17, 16 },
            { 4, 5, 6, 7 },
            { 23, 22, 21, 20 },
            { 0, 1, 2, 3 },
            { 12, 13, 14, 15 },
            { 11, 10, 9, 8 }
        };

        void fillNumber(LedStripFrame<NUM_LEDS> *frame, int previous, int current, int fade, int max_intensity, int digit_place);
};

#endif