#include "BinaryAFace.h"

int NUMBER[10][4] = {
    { 0, 0, 0, 0 },
    { 0, 0, 0, 1 },
    { 0, 0, 1, 0 },
    { 0, 0, 1, 1 },
    { 0, 1, 0, 0 },
    { 0, 1, 0, 1 },
    { 0, 1, 1, 0 },
    { 0, 1, 1, 1 },
    { 1, 0, 0, 0 },
    { 1, 0, 0, 1 }
};

LedStripFrame<NUM_LEDS> BinaryAFace::frameFor(const int hour, const int minute, const int second) {
    LedStripFrame<NUM_LEDS> frame;

    int second_unit = second % 10;
    int second_ten  = second / 10;
    int minute_unit = minute % 10;
    int minute_ten  = minute / 10;
    int hour_unit   = hour % 10;
    int hour_ten    = hour / 10;

    int previous_hour_ten    = this->previous_hour / 10;
    int previous_hour_unit   = this->previous_hour % 10;
    int previous_minute_ten  = this->previous_minute / 10;
    int previous_minute_unit = this->previous_minute % 10;
    int previous_second_ten  = this->previous_second / 10;
    int previous_second_unit = this->previous_second % 10;

    fillNumber(&frame, previous_hour_ten,    hour_ten,    this->fade, this->max_intensity, 0);
    fillNumber(&frame, previous_hour_unit,   hour_unit,   this->fade, this->max_intensity, 1);
    fillNumber(&frame, previous_minute_ten,  minute_ten,  this->fade, this->max_intensity, 2);
    fillNumber(&frame, previous_minute_unit, minute_unit, this->fade, this->max_intensity, 3);
    fillNumber(&frame, previous_second_ten,  second_ten,  this->fade, this->max_intensity, 4);
    fillNumber(&frame, previous_second_unit, second_unit, this->fade, this->max_intensity, 5);

    if(this->previous_second != second || this->previous_minute != minute || this->previous_hour != hour) {
        fade = fade + this->fading_step;
        if(fade > (this->max_intensity - this->min_intensity)) {
            fade = this->min_intensity;
            this->previous_second = second;
            this->previous_minute = minute;
            this->previous_hour = hour;
            this->hue_offset = (this->hue_offset + 1)%(255);
        }
    }

    frame.setDuration(10);

    return frame;
}

void BinaryAFace::fillNumber(LedStripFrame<NUM_LEDS> *frame, int previous, int current, int fade, int max_intensity, int digit_place) {
    int hue = (this->hue_offset + 255/6*digit_place)%255;

    for(int i=0; i<4; i++) {
        if(NUMBER[current][i] == 1 && NUMBER[previous][i] == 0)
            frame->setPixelAt(DIGIT[digit_place][i], CHSV(hue, 255, fade));
        
        if(NUMBER[previous][i] == 1 && NUMBER[current][i] == 0)
            frame->setPixelAt(DIGIT[digit_place][i], CHSV(hue, 255, this->max_intensity - fade));

        if(NUMBER[previous][i] == 1 && NUMBER[current][i] == 1)
            frame->setPixelAt(DIGIT[digit_place][i], CHSV(hue, 255, this->max_intensity));
        
        if(NUMBER[current][i] == 0 && NUMBER[previous][i] == 0)
            frame->setPixelAt(DIGIT[digit_place][i], CHSV(hue, 255, this->min_intensity));
    }
}