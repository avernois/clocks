#ifndef __TWO_BY_TWO_UNI_FRAME_H__
#define __TWO_BY_TWO_UNI_FRAME_H__

#include "ledstrip/LedStripFrame.h"
#include "ClockFace.h"

#define NUM_LEDS 4

class TwoByTwoUniFace : public ClockFace<LedStripFrame<NUM_LEDS>> {

    public:
        LedStripFrame<NUM_LEDS> frameFor(const int hour, const int minute, const int second) {
            LedStripFrame<NUM_LEDS> frame;
            String time = "";
            time = time + hour + ":" + minute + ":" + second;
            Serial.println(time);
            frame.setPixelAt(0, CHSV(second * 255/60, 255, value));
            frame.setPixelAt(3, CHSV(second * 255/60, 255, value));
            frame.setPixelAt(1, CHSV(second * 255/60, 255, value));
            frame.setPixelAt(2, CHSV(second * 255/60, 255, value));
        
            frame.setDuration(500);

            return frame;
        };

    private:
        const int value = 255;
};

#endif