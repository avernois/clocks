#include <ZoneOffset.h>
#include <WifiLedClock.h>

#include <ledstrip/WS2812bDisplay.h>

#include "TwoByTwoFace.h"
#include "TwoByTwoUniFace.h"

HTTPClient httpClient;
String host = "zone.crafting-labs.fr";

Zone zone(httpClient, host);

#define NUM_LEDS 4
#define DATA_PIN 2

TwoByTwoFace* face = new TwoByTwoFace();
TwoByTwoUniFace* uniFace = new TwoByTwoUniFace();

WS2812bDisplay<DATA_PIN, NUM_LEDS> *display = new WS2812bDisplay<DATA_PIN, NUM_LEDS>();
WifiLedClock<LedStripFrame<NUM_LEDS>>* ledClock = new WifiLedClock<LedStripFrame<NUM_LEDS>>("fourclock", face, display, &zone);

void setup() {
    Serial.begin(115200);
    ledClock->addFace(uniFace);
    ledClock->setup();
}

void loop() {
    ledClock->loop();

    Serial.print("Free heap: ");
    Serial.println(ESP.getFreeHeap());
}
