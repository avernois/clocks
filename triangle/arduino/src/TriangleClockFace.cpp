#include "TriangleClockFace.h"

#define MAX_HUE_VALUE 255
int color_shift = 0;

const int NB_UNITS_LEDS = 9;
const int NB_UNITS = 10;
int HOUR_UNIT_STRIP[NB_UNITS_LEDS] = 
    {  0,  1,  2, 19,  3, 18,  4, 17, 20 };
int MINUTE_UNIT_STRIP[NB_UNITS_LEDS] = 
    { 10,  9,  8, 11,  7, 12,  6, 13, 26 };
int *UNIT[NB_UNITS] = 
    { new int[NB_UNITS_LEDS] {  0,  0,  0,  0,  0,  0,  0,  0,  0 },
      new int[NB_UNITS_LEDS] {  1,  0,  0,  0,  0,  0,  0,  0,  0 },
      new int[NB_UNITS_LEDS] {  0,  0,  1,  1,  0,  0,  0,  0,  0 },
      new int[NB_UNITS_LEDS] {  0,  1,  0,  0,  1,  1,  0,  0,  0 },
      new int[NB_UNITS_LEDS] {  0,  0,  1,  1,  1,  1,  0,  0,  0 },
      new int[NB_UNITS_LEDS] {  1,  1,  0,  0,  0,  0,  1,  1,  1 },
      new int[NB_UNITS_LEDS] {  1,  1,  0,  0,  1,  1,  1,  0,  1 },
      new int[NB_UNITS_LEDS] {  1,  1,  1,  1,  1,  1,  0,  1,  0 },
      new int[NB_UNITS_LEDS] {  1,  1,  1,  1,  1,  1,  1,  0,  1 },
      new int[NB_UNITS_LEDS] {  1,  1,  1,  1,  1,  1,  1,  1,  1 }
    };

const int NB_HOUR_TENS_LEDS = 4;
const int NB_HOUR_TENS      = 3;
int HOUR_TENS_STRIP[NB_HOUR_TENS_LEDS] = 
    { 31, 30, 29, 32 };
int *HOUR_TENS[NB_HOUR_TENS] = 
    { new int[NB_HOUR_TENS_LEDS] {  0,  0,  0,  0 },
      new int[NB_HOUR_TENS_LEDS] {  1,  0,  0,  0 },
      new int[NB_HOUR_TENS_LEDS] {  0,  0,  1,  1 }
    };

const int NB_MINUTE_TENS_LEDS = 5;
const int NB_MINUTE_TENS = 6;
int MINUTE_TENS_STRIP[NB_MINUTE_TENS_LEDS] = 
    { 27, 28, 34, 33, 35 };
int *MINUTE_TENS[NB_MINUTE_TENS] = 
    { new int[NB_MINUTE_TENS_LEDS] {  0,  0,  0,  0,  0 },
      new int[NB_MINUTE_TENS_LEDS] {  0,  0,  1,  0,  0 },
      new int[NB_MINUTE_TENS_LEDS] {  0,  1,  0,  1,  0 },
      new int[NB_MINUTE_TENS_LEDS] {  1,  0,  1,  0,  1 },
      new int[NB_MINUTE_TENS_LEDS] {  1,  1,  0,  1,  1 },
      new int[NB_MINUTE_TENS_LEDS] {  1,  1,  1,  1,  1 }
    };

TriangleClockFace::Digit minuteUnits = { NB_UNITS_LEDS, MINUTE_UNIT_STRIP, NB_UNITS, UNIT, MAX_HUE_VALUE/3 };
TriangleClockFace::Digit minuteTens = { NB_MINUTE_TENS_LEDS, MINUTE_TENS_STRIP, NB_MINUTE_TENS, MINUTE_TENS, MAX_HUE_VALUE/3 };
TriangleClockFace::Digit hourUnits = { NB_UNITS_LEDS, HOUR_UNIT_STRIP, NB_UNITS, UNIT, 0 };
TriangleClockFace::Digit hourTens = { NB_HOUR_TENS_LEDS, HOUR_TENS_STRIP, NB_HOUR_TENS, HOUR_TENS, 0 };

LedStripFrame<NUM_LEDS> TriangleClockFace::frameFor(const int hour, const int minute, const int second) {
    LedStripFrame<NUM_LEDS> frame;
    
    int minute_tens = minute / 10;
    int minute_unit = minute % 10;
    int hour_tens = hour / 10;
    int hour_unit = hour % 10;
    
    int previous_hour_tens = this->previous_hour / 10;
    int previous_hour_unit = this->previous_hour % 10;
    int previous_minute_tens = this->previous_minute / 10;
    int previous_minute_unit = this->previous_minute % 10;


    displayValue(&frame, minuteUnits, minute_unit, previous_minute_unit);
    displayValue(&frame, minuteTens, minute_tens, previous_minute_tens);
    displayValue(&frame, hourUnits, hour_unit, previous_hour_unit);
    displayValue(&frame, hourTens, hour_tens, previous_hour_tens);
    
    hue_change = (hue_change + 1) % 3;
    if(hue_change == 1) {
        color_shift = (color_shift + 1) % MAX_HUE_VALUE;
    }

    if(this->previous_minute != minute || this->previous_hour != hour) {
        fade = fade + 4;
        if(fade > this->intensity - this->min_intensity) {
            fade = this->min_intensity;

            this->previous_minute = minute;
            this->previous_hour = hour;
        }
    }

    frame.setDuration(50);

    return frame;
}

void TriangleClockFace::displayValue(LedStripFrame<NUM_LEDS> *frame, Digit digit, int value, int previous_value) {
    for (int i=0; i<digit.nbLeds; i++) {

        if(digit.digits[previous_value][i] == 1 && digit.digits[value][i] == 1)
            frame->setPixelAt(digit.digitStrip[i], CHSV((color_shift + gradient_step*i + digit.colorShift) % MAX_HUE_VALUE, 255, this->intensity));

        if(digit.digits[previous_value][i] == 0 && digit.digits[value][i] == 0)
            frame->setPixelAt(digit.digitStrip[i], CHSV((color_shift + gradient_step*i + digit.colorShift) % MAX_HUE_VALUE, 255, this->min_intensity));

        if(digit.digits[previous_value][i] == 0 && digit.digits[value][i] == 1)
            frame->setPixelAt(digit.digitStrip[i], CHSV((color_shift + gradient_step*i + digit.colorShift) % MAX_HUE_VALUE, 255, fade));

        if(digit.digits[previous_value][i] == 1 && digit.digits[value][i] == 0)
            frame->setPixelAt(digit.digitStrip[i], CHSV((color_shift + gradient_step*i + digit.colorShift) % MAX_HUE_VALUE, 255, this->intensity - fade));

    }
}