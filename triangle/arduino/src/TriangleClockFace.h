#ifndef __have__TriangleClockDisplay_h__
#define __have__TriangleClockDisplay_h__

#include <ledstrip/LedStripFrame.h>
#include <ClockFace.h>

#define NUM_LEDS 36

class TriangleClockFace: public ClockFace<LedStripFrame<NUM_LEDS>> {
    public:
        LedStripFrame<NUM_LEDS> frameFor(const int hour, const int minute, const int second);
        struct Digit { 
            int nbLeds;
            int* digitStrip;
            int nbValues;
            int** digits;
            int colorShift;
        };

    private: 
        int previous_minute = 0;
        int previous_hour = 0;
        int fade = 0;
        const int intensity = 160;
        const int min_intensity = 0;
        int hue_change = 0;
        const int gradient_step = 0;

        void displayValue(LedStripFrame<NUM_LEDS> *frame, Digit digit, int value, int previous_value);
};

#endif